<?php
/**
 * @file
 * Views plugin definitions.
 */

/**
 * Implements hook_views_plugins().
 */
function views_content_breadcrumb_views_plugins() {
  $plugins = array();
  $plugins['display_extender']['breadcrumb'] = array(
    'title' => t('Breadcrumb'),
    'help' => t('Provides custom breadcrumb for views.'),
    'handler' => 'views_content_breadcrumb_plugin_display_extender',
    'path' => drupal_get_path('module', 'views_content_breadcrumb') . '/views',
    'theme path' => drupal_get_path('module', 'views') . '/theme',
    'theme' => 'views_view',
    'register theme' => FALSE,
    'enabled' => TRUE,
  );

  return $plugins;
}
