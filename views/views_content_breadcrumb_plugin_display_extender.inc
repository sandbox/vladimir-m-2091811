<?php
/**
 * @file
 * Custom display extender plugin for Views.
 */

/**
 * Class views_content_breadcrumb_plugin_display_extender.
 */
class views_content_breadcrumb_plugin_display_extender extends views_plugin_display_extender {
  /**
   * Check if page has breadcrumb.
   */
  protected function has_breadcrumb() {
    return views_content_breadcrumb_views_display_has_breadcrumb($this->display);
  }

  /**
   * Define options.
   */
  function options_definition_alter(&$options) {
    $options['breadcrumb_status'] = array('default' => array());
    $options['breadcrumb_titles'] = array('default' => array());
    $options['breadcrumb_paths'] = array('default' => array());
  }

  /**
   * Options summary.
   */
  function options_summary(&$categories, &$options) {
    $status = $this->display->get_option('breadcrumb_status');

    $categories['breadcrumb'] = array(
      'title' => t('Breadcrumb'),
      'column' => 'second',
    );
    $options['use_breadcrumb'] = array(
      'category' => 'breadcrumb',
      'title' => t('Use breadcrumb'),
      'value' => !empty($status) ? t('Override') : t('No'),
    );
  }

  /**
   * Define form options.
   */
  function options_form(&$form, &$form_state) {
    if ($form_state['section'] == 'use_breadcrumb') {
      $form['#title'] .= t('Breadcrumb for this display');
      // Titles.
      $form['breadcrumb_titles'] = array(
        '#title' => t('Titles'),
        '#type' => 'textarea',
        '#description' => t('A list of titles for the breadcrumb links, one on each line. Use <em>@placeholder</em> placeholder to escape title.', array('@placeholder' => '<none>')),
        '#default_value' => $this->display->get_option('breadcrumb_titles'),
        '#required' => TRUE,
      );
      // Paths.
      $form['breadcrumb_paths'] = array(
        '#title' => t('Paths'),
        '#type' => 'textarea',
        '#description' => t('A list of Drupal paths for the breadcrumb links, one on each line. Use <em>@placeholder</em> placeholder to escape path.', array('@placeholder' => '<none>')),
        '#default_value' => $this->display->get_option('breadcrumb_paths'),
        '#required' => TRUE,
      );
      $form['breadcrumb_status'] = array(
        '#type' => 'checkbox',
        '#title' => t('Enable breadcrumb for this display?'),
        '#default_value' => $this->display->get_option('breadcrumb_status'),
      );
    }
  }

  /**
   *  Form submit handler.
   */
  function options_submit(&$form, &$form_state) {
    if (!empty($form_state['values']['breadcrumb_titles'])) {
      $this->display->set_option('breadcrumb_status', $form_state['values']['breadcrumb_status']);
      $this->display->set_option('breadcrumb_titles', $form_state['values']['breadcrumb_titles']);
      $this->display->set_option('breadcrumb_paths', $form_state['values']['breadcrumb_paths']);
    }
  }
}
